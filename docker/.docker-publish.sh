#!/bin/bash
#SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd || exit 1 )"
#group_url="registry.gitlab.com/mrconfetti"
#repo="$group_url/medical-website"
#branch="$(git branch --show-current)"
#[ -z "$1" ] && epoch="$(date +%s)" || epoch="$1"

#docker buildx create --use --buildkitd-flags "--allow-insecure-entitlement network.host"
#docker buildx build --platform linux/amd64,linux/arm64 --push --network=host -t "${repo}/${branch}:${epoch}" -f "${SCRIPT_DIR}/Dockerfile" "${SCRIPT_DIR}/.." || exit 1

docker buildx create --name multiarch --driver docker-container --use
docker buildx build --platform linux/amd64,linux/arm64 --push -t "europe-west1-docker.pkg.dev/expanded-genius-348314/alexandra-site/$1" -f docker/Dockerfile .

echo "The tag is $1"
