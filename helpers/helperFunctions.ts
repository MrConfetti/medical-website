import { GAPI_KEY } from "~~/constants/constants";

export function scrollToElement(id: string) {
  const el = document.getElementById(id);
  if (el) {
    el.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest",
    });
  }
}

async function getGoogleDriveFile(fileId: string) {
  const FILE_URL = `https://www.googleapis.com/drive/v3/files/${fileId}/export?mimeType=text/plain&key=${GAPI_KEY}`;
  const res = await fetch(FILE_URL);
  if (!res.ok) {
    throw new Error("Network response was not ok " + res.statusText);
  }
  return res;
}

export async function getGoogleDriveFileContent(fileId: string) {
  const res = await getGoogleDriveFile(fileId);
  const markdownContent = await res.text();
  return markdownContent.trim();
}
