// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  extends: ["nuxt-seo-kit"],
  app: {
    head: {
      titleTemplate: "%siteName %titleSeparator %s",
    },
  },
  modules: [
    "@nuxtjs/google-fonts",
    "@nuxt/image-edge",
    "@zadigetvoltaire/nuxt-gtm",
  ],
  build: {
    transpile: ["primevue"],
  },
  css: [
    "primevue/resources/themes/lara-light-blue/theme.css",
    "primevue/resources/primevue.css",
    "primeicons/primeicons.css",
    "primeflex/primeflex.css",
    "~/assets/style/main.css",
  ],
  runtimeConfig: {
    public: {
      siteUrl: "https://www.kinderarzterfurt.de",
      siteName: "Kinderarzterfurt",
      siteDescription: "Praxis Für Kinder-und Jugendmedizin",
      language: "de",
    },
  },
  image: {
    screens: {
      xs: 320,
      sm: 640,
      md: 768,
      lg: 1024,
      xl: 1280,
      xxl: 1536,
      "2xl": 1536,
    },
  },
  googleFonts: {
    preload: true,
    families: {
      "Baloo 2": [400, 700],
      Jaldi: [400, 700],
      Montserrat: [400, 500, 700],
      "Life Savers": [700],
    },
  },
  schemaOrg: {
    host: "https://www.kinderarzterfurt.de",
  },
  gtm: {
    id: "GTM-W97CPSSR",
    defer: false,
    compatibility: false,
    nonce: "2726c7f26c",
    enabled: true,
    debug: false,
    loadScript: true,
    enableRouterSync: true,
    trackOnNextTick: false,
    devtools: false,
  },
});
