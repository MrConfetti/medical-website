import { defineNuxtPlugin } from "#app";
import PrimeVue from "primevue/config";
import Button from "primevue/button";
import Fieldset from "primevue/fieldset";
import Menubar from "primevue/menubar";
import InputText from "primevue/inputtext";
import Calendar from "primevue/calendar";
import Textarea from "primevue/textarea";
import Toast from "primevue/toast";
import Dialog from 'primevue/dialog';


export default defineNuxtPlugin((nuxtApp) => {
    nuxtApp.vueApp.use(PrimeVue, { ripple: true });
    nuxtApp.vueApp.component("Button", Button);
    nuxtApp.vueApp.component("Fieldset", Fieldset);
    nuxtApp.vueApp.component("Menubar", Menubar);
    nuxtApp.vueApp.component("InputText", InputText);
    nuxtApp.vueApp.component("Calendar", Calendar);
    nuxtApp.vueApp.component("Textarea", Textarea);
    nuxtApp.vueApp.component("Toast", Toast);
    nuxtApp.vueApp.component("Dialog", Dialog);
});